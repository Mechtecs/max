export class PhotonResult {
  features: {
    geometry: {
      coordinates: number[];
      type: string;
    };
    type: string;
    properties: {
      country: string;
      osm_key: string;
      housenumber: string;
      city: string;
      street: string;
      osm_value: string;
      postcode: string;
      state:  string;
    };
  }[];

  public static toString(properties: {
    country: string;
    osm_key: string;
    housenumber: string;
    city: string;
    street: string;
    osm_value: string;
    postcode: string;
    state:  string;
  }) {
    const rep = [];
    if (properties.street && properties.housenumber) {
      rep.push(`${properties.street} ${properties.housenumber},`);
    } else if (properties.street && !properties.housenumber) {
      rep.push(`${properties.street},`);
    }
    if (properties.postcode) {
      rep.push(properties.postcode);
    }
    if (properties.city) {
      rep.push(properties.city);
    }
    if (properties.state) {
      rep.push(properties.state);
    }
    if (properties.country) {
      rep.push(properties.country);
    }
    return rep.join(' ');
  }
}
