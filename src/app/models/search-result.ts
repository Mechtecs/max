import {TMEvent} from './TMEvent';

export class SearchResult {
  page: {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
  };

  _embedded: {
    events: TMEvent[];
  };
}
