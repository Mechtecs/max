import { Component, OnInit } from '@angular/core';
import { TicketmasterService } from '../../services/ticketmaster.service';
import { ActivatedRoute } from '@angular/router';
import { TMEvent } from '../../models/TMEvent';
import { latLng, Layer, marker, tileLayer } from 'leaflet';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  public options = null;
  private event: TMEvent;
  private layers: Layer[];
  address: any;

  constructor(
    private ticketmaster: TicketmasterService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.getDetail(params.id));
  }

  private getDetail(id) {
    this.ticketmaster.detail(id).subscribe((event: TMEvent) => {
      this.event = event;
      const location = event._embedded.venues[0];
      this.options = {
        layers: [
          tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18, attribution: '...'}),
        ],
        zoom: 12,
        center: latLng(location.location.latitude, location.location.longitude)
      };
      this.layers = [
        marker([location.location.latitude, location.location.longitude])
      ];
      this.address = location;
    });
  }

  getLargestImage(event: TMEvent) {
    if (event.images) {
      return event.images.reduce((max, small) => {
        return max.width * max.height >= small.width * small.height ? max : small;
      });
    } else {
      return null;
    }
  }

  getDateAsString(event: {
    name: string; type: string; id: string; test: boolean; url: string; locale: string; location: { latitude: number; longitude: number }; images: { url: string; ratio: string; width: number; height: number; fallback: boolean; attribution: string }[]; dates: { start: { dateTime: string; dateTBD: boolean; dateTBA: boolean; timeTBA: boolean; noSpecificTime: boolean }; end: { dateTime: string; approximate: boolean; noSpecificTime: boolean }; access: { startDateTime: string; startApproximate: boolean; endDateTime: string; endApproximate: boolean }; status: { code: number }; timezone: string; spanMultipleDays: boolean }; sales: { public: { startDateTime: string; endDateTime: string; startTBD: boolean }; presales: { name: string; description: string; url: string; startDateTime: string; endDateTime: string }[] }; info: string; pleaseNote: string; priceRanges: { type: string; currency: string; min: number; max: number }[]; promoter: { id: string; name: string; description: string }; promoters: { id: string; name: string; description: string }[]; outlets: { url: string; type: string }[]; products: { name: string; id: string; url: string; type: string }[]; seatmap: { staticUrl: string }; accessibility: { info: string }; ticketLimit: { info: string; infos: any }; classifications: { primary: boolean; segment: { id: string; name: string; locale: string; genres: { id: string; name: string; locale: string; subGenres: { id: string; name: string; locale: string }[] }[]; genre: { id: string; name: string; locale: string }; subGenre: { id: string; name: string; locale: string }; type: { subTypes: { id: string; name: string; locale: string }[]; id: string; name: string; locale: string }; subType: { id: string; name: string; locale: string }; family: boolean } }[]; place: { area: { name: string }; address: { line1: string; line2: string; line3: string }; city: { name: string }; state: { stateCode: string; name: string }; country: { countryCode: string; name: string }; postalCode: string; location: { longitude: number; latitude: number }; name: string }; externalLinks: any; aliases: string[]; localizedAliases: any
  }) {
    const date = new Date(event.dates.start.dateTime);
    return date.toLocaleDateString();
  }
}
