import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AviationEdgeService } from '../../../services/aviation-edge.service';

@Component({
  selector: 'app-flight-check-choose-airports',
  templateUrl: './flight-check-choose-airports.component.html',
  styleUrls: ['./flight-check-choose-airports.component.scss']
})
export class FlightCheckChooseAirportsComponent implements OnInit {

  @Input() state: string;
  @Output() stateChange = new EventEmitter();

  @Input() lat: number;
  @Input() lon: number;

  startAirports: any;
  startAirport: any;
  destinationAirports: any;
  destinationAirport: any;

  constructor(
    private aviation: AviationEdgeService,
  ) {
  }

  ngOnInit() {
    if (localStorage.getItem('userLocationLat') && localStorage.getItem('userLocationLon')) {
      this.aviation.nearestAirports(+localStorage.getItem('userLocationLat'), +localStorage.getItem('userLocationLon')).subscribe((startAirports) => {
        this.startAirports = (startAirports as any[]).filter((element) => element.codeIcaoAirport);
      });
      this.aviation.nearestAirports(this.lat, this.lon).subscribe((destinationAirports) => {
        this.destinationAirports = (destinationAirports as any[]).filter((element) => element.codeIcaoAirport);
      });
    } else {
      this.state = 'askLocation';
      this.stateChange.emit(this.state);
    }
  }

  checkReady() {
    return this.startAirport && this.destinationAirport;
  }

  apply() {
    if (!this.checkReady()) {
      return;
    }
    localStorage.setItem('startAirport', JSON.stringify(this.startAirport));
    localStorage.setItem('destinationAirport', JSON.stringify(this.destinationAirport));

    this.state = 'calculateRoute';
    this.stateChange.emit('calculateRoute');
  }

}
