import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightCheckChooseAirportsComponent } from './flight-check-choose-airports.component';

describe('FlightCheckChooseAirportsComponent', () => {
  let component: FlightCheckChooseAirportsComponent;
  let fixture: ComponentFixture<FlightCheckChooseAirportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightCheckChooseAirportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightCheckChooseAirportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
