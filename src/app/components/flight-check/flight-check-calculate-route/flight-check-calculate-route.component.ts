import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LufthansaService } from '../../../services/lufthansa.service';

@Component({
  selector: 'app-flight-check-calculate-route',
  templateUrl: './flight-check-calculate-route.component.html',
  styleUrls: ['./flight-check-calculate-route.component.scss']
})
export class FlightCheckCalculateRouteComponent implements OnInit {

  @Input() state: string;
  @Output() stateChange = new EventEmitter();
  @Input() date: string;
  @Input() reverse = false;

  flightResult: any;

  constructor(
    private lufthansa: LufthansaService,
  ) { }

  ngOnInit() {
    let startAirport;
    let destinationAirport;
    try {
      startAirport = JSON.parse(localStorage.getItem('startAirport')).codeIataAirport;
      destinationAirport = JSON.parse(localStorage.getItem('destinationAirport')).codeIataAirport;
      if (!startAirport || !destinationAirport) {
        this.state = 'askLocation';
        this.stateChange.emit(this.state);
        return;
      }
    } catch (e) {
      this.state = 'askLocation';
      this.stateChange.emit(this.state);
      return;
    }

    if (this.reverse) {
      const tmp = destinationAirport;
      destinationAirport = startAirport;
      startAirport = tmp;
    }

    this.lufthansa.calculateRoute(startAirport, destinationAirport, this.date).subscribe((result) => {
      console.log(result);
      this.flightResult = result;
    });
  }

  getTime(duration: string) {
    const durationArr = duration.match(/.*([0-9]+)?PT?([0-9]+)?H?([0-9]+)?M?/);
    console.log(durationArr[3]);
    let final = '';
    if (durationArr[1]) {
      final += duration[1] + ' Tag(e) ';
    }
    if (durationArr[2]) {
      final += duration[2] + ' Stunde(n) ';
    }
    if (durationArr[3]) {
      final += duration[3] + ' Minute(n) ';
    }
    return final;
  }
}
