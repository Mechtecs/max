import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightCheckCalculateRouteComponent } from './flight-check-calculate-route.component';

describe('FlightCheckCalculateRouteComponent', () => {
  let component: FlightCheckCalculateRouteComponent;
  let fixture: ComponentFixture<FlightCheckCalculateRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightCheckCalculateRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightCheckCalculateRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
