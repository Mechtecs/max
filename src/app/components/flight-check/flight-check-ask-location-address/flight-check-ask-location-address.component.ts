import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PhotonService } from '../../../services/photon.service';
import { PhotonResult } from '../../../models/photon-result';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-flight-check-ask-location-address',
  templateUrl: './flight-check-ask-location-address.component.html',
  styleUrls: ['./flight-check-ask-location-address.component.scss']
})
export class FlightCheckAskLocationAddressComponent implements OnInit {

  @Input() state: string;
  @Output() stateChange = new EventEmitter();

  photonResult: PhotonResult;
  private photonSubscription: Subscription;
  searchText: string;
  private timeoutID: number;
  photonAddress: any;

  constructor(
    private photon: PhotonService
  ) { }

  ngOnInit() {
  }

  searchDelay(text: string) {
    if (this.timeoutID) {
      clearTimeout(this.timeoutID);
      this.timeoutID = null;
    }
    this.timeoutID = setTimeout(() => {
      this.search(text);
    }, 300);
  }

  apply() {
    localStorage.setItem('userLocationLat', this.photonAddress[1]);
    localStorage.setItem('userLocationLon', this.photonAddress[0]);

    this.state = 'calculateRoute';
    this.stateChange.emit('calculateRoute');
  }

  search(text: string) {
    if (this.photonSubscription && !this.photonSubscription.closed) {
      this.photonSubscription.unsubscribe();
    }
    this.photonSubscription = this.photon.autoComplete(text).subscribe((value => {
      this.photonAddress = null;
      this.photonResult = value as PhotonResult;
    }));
  }

  photonString(properties: {
    country: string;
    osm_key: string;
    housenumber: string;
    city: string;
    street: string;
    osm_value: string;
    postcode: string;
    state:  string;
  }) {
    const rep = [];
    if (properties.street && properties.housenumber) {
      rep.push(`${properties.street} ${properties.housenumber},`);
    } else if (properties.street && !properties.housenumber) {
      rep.push(`${properties.street},`);
    }
    if (properties.postcode) {
      rep.push(properties.postcode);
    }
    if (properties.city) {
      rep.push(properties.city);
    }
    if (properties.state) {
      rep.push(properties.state);
    }
    if (properties.country) {
      rep.push(properties.country);
    }
    return rep.join(' ');
  }

}
