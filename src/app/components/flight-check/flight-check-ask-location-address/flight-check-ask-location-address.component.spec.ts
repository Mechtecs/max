import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightCheckAskLocationAddressComponent } from './flight-check-ask-location-address.component';

describe('FlightCheckAskLocationAddressComponent', () => {
  let component: FlightCheckAskLocationAddressComponent;
  let fixture: ComponentFixture<FlightCheckAskLocationAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightCheckAskLocationAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightCheckAskLocationAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
