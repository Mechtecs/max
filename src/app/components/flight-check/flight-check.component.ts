import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-flight-check',
  templateUrl: './flight-check.component.html',
  styleUrls: ['./flight-check.component.scss']
})
export class FlightCheckComponent implements OnInit {

  @Input() lat: number;
  @Input() lon: number;
  public state: string;

  constructor() { }

  ngOnInit() {
    if (localStorage.getItem('userLocationLat')) {
      this.state = 'chooseAirports';
    } else {
      this.state = 'askLocation';
    }
  }

  getRelativeDate(number: number) {
    const date = new Date();
    return date.getFullYear().toString() + '-' + ((date.getMonth() + 1).toString().length === 1 ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1 ).toString()) + '-' + (date.getDate() + number + 1).toString();
  }
}
