import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-flight-check-ask-location',
  templateUrl: './flight-check-ask-location.component.html',
  styleUrls: ['./flight-check-ask-location.component.scss']
})
export class FlightCheckAskLocationComponent implements OnInit {

  @Input() state: string;
  @Output() stateChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  askForLocation() {
    this.state = 'askLocationAddress';
    this.stateChange.emit(this.state);
  }

  canDoGeolocation() {
    return 'geolocation' in navigator;
  }

  getBrowserLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      localStorage.setItem('userLocationLat', position.coords.latitude.toString());
      localStorage.setItem('userLocationLon', position.coords.longitude.toString());
    });

    this.state = 'chooseAirports';
    this.stateChange.emit(this.state);
  }
}
