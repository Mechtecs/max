import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightCheckAskLocationComponent } from './flight-check-ask-location.component';

describe('FlightCheckAskLocationComponent', () => {
  let component: FlightCheckAskLocationComponent;
  let fixture: ComponentFixture<FlightCheckAskLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightCheckAskLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightCheckAskLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
