import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightCheckComponent } from './flight-check.component';

describe('FlightCheckComponent', () => {
  let component: FlightCheckComponent;
  let fixture: ComponentFixture<FlightCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
