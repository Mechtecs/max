import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {
  MzButtonModule,
  MzCardModule,
  MzCollectionModule,
  MzIconMdiModule,
  MzIconModule,
  MzInputModule,
  MzNavbarModule, MzParallaxModule, MzProgressModule, MzSelectModule, MzSpinnerModule
} from 'ngx-materialize';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DetailPageComponent } from './components/detail-page/detail-page.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FlightCheckComponent } from './components/flight-check/flight-check.component';
import { FlightCheckAskLocationComponent } from './components/flight-check/flight-check-ask-location/flight-check-ask-location.component';
import { FlightCheckChooseAirportsComponent } from './components/flight-check/flight-check-choose-airports/flight-check-choose-airports.component';
import { FlightCheckAskLocationAddressComponent } from './components/flight-check/flight-check-ask-location-address/flight-check-ask-location-address.component';
import { FlightCheckCalculateRouteComponent } from './components/flight-check/flight-check-calculate-route/flight-check-calculate-route.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    PageNotFoundComponent,
    NavbarComponent,
    SearchResultComponent,
    DetailPageComponent,
    FlightCheckComponent,
    FlightCheckAskLocationComponent,
    FlightCheckChooseAirportsComponent,
    FlightCheckAskLocationAddressComponent,
    FlightCheckCalculateRouteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,

    LeafletModule.forRoot(),

    MzNavbarModule,
    MzInputModule,
    MzIconMdiModule,
    MzIconModule,
    MzCollectionModule,
    MzCardModule,
    MzButtonModule,
    MzProgressModule,
    MzParallaxModule,
    MzSelectModule,
    MzSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
