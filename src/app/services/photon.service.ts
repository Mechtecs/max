import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhotonService {

  constructor(
    private http: HttpClient,
  ) { }

  autoComplete(text: string) {
    return this.http.get(`http://photon.komoot.de/api/`, {
      params: {
        q: text
      }
    });
  }
}
