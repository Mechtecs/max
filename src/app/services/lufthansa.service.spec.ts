import { TestBed } from '@angular/core/testing';

import { LufthansaService } from './lufthansa.service';

describe('LufthansaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LufthansaService = TestBed.get(LufthansaService);
    expect(service).toBeTruthy();
  });
});
