import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LufthansaService {

  constructor(
    private http: HttpClient,
  ) { }

  calculateRoute(from: string, to: string, date: string) {
    return this.http.get(`${env.middlewareURL}/index.php`, {
      params: {
        api: 'flight',
        command: 'show',
        c1: from,
        c2: to,
        c3: date,
      }
    });
  }
}
