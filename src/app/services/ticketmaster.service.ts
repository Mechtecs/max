import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketmasterService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public detail(id: string): Observable<object> {
    return this.http.get(`${env.middlewareURL}/index.php`, {
      params: {
        command: 'detail',
        c1: id
      }
    });
  }

  public search(text: string): Observable<object> {
    return this.http.get(`${env.middlewareURL}/index.php`, {
      params: {
        q: text
      }
    });
  }

}
