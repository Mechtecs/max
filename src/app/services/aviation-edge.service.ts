import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AviationEdgeService {

  constructor(
    private http: HttpClient,
  ) { }

  nearestAirports(lat: number, lon: number) {
    return this.http.get(`${env.middlewareURL}/index.php`, {
      params: {
        api: 'routing',
        command: 'show',
        c1: lat.toString(),
        c2: lon.toString(),
        c3: '100',
      }
    });
  }
}
