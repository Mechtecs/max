import { TestBed } from '@angular/core/testing';

import { AviationEdgeService } from './aviation-edge.service';

describe('AviationEdgeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AviationEdgeService = TestBed.get(AviationEdgeService);
    expect(service).toBeTruthy();
  });
});
